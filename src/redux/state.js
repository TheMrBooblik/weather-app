import React from "react";
import {renderEntireTree} from "../render";

let notifications = {
    tooShort: "Login/password should be at least 6 characters long",
    tooLong: "Login/password can't be more, than 100 characters",
    wrongCreds: "Incorrect login or password",
    success: "",
};

let state = {
    weather: {
        url: "http://api.openweathermap.org/data/2.5/weather?q=",
        imgUrl: "http://openweathermap.org/img/wn/",
        city: 'kharkiv',
        apiKey: '70d78d7ab58c580e0f6b22be23758237',
        data: {},
    },
    notification: '',
    user: {
        login: '',
        password: '',
        notification: ''
    },
    isAuth: false,
};

export let changeAuthStatus = (authStatus) => {
    state.isAuth = authStatus;
    renderEntireTree(state, authStatus);
};

export let setWeatherData = (data) => {
    state.weather.data = data;
};

export let setNotification = (type) => {
    state.notification = notifications[type];
};

export default state;
