import React from 'react'
import s from './Icon.module.css'

const Icon = (props) => {

    return (
        <div className={s.icon}>
            <img src={props.state.weather.imgUrl + props.weather[0].icon + "@2x.png"} alt="" />
        </div>
    )

};

export default Icon;
