import React from 'react'
import s from './City.module.css'

const City = (props) => {
    return (
        <div className={s.city}>
            <span>{props.name}</span>
        </div>
    )
};

export default City;
