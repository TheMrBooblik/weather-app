import React from 'react'
import s from './Weather.module.css'
import City from "./City/City";
import Temperature from "./Temperature/Temperature";
import {Redirect} from "react-router";
import Icon from "./Icon/Icon";

const Weather = (props) => {

    if (props.state.isAuth === false) return <Redirect to={"/login"}/>;

    return (
        <div className={s.weather}>
            <div className={s.weatherBlock}>
                <Icon state={props.state} weather={props.currentWeather.weather}/>
                <Temperature degrees={props.currentWeather.main} />
                <City name={props.currentWeather.name} />
            </div>
        </div>
    )

};

export default Weather;
