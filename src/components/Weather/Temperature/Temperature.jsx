import React from 'react'
import s from './Temperature.module.css'

const Temperature = (props) => {

    return (
        <div className={s.temperature}>
            <span>{props.degrees.temp}</span>
        </div>
    )
}

export default Temperature;
