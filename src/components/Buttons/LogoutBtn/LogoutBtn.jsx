import React from 'react'
import {NavLink} from "react-router-dom";
import {changeAuthStatus} from "../../../redux/state";

const LogoutBtn = (props) => {
    return (
        <NavLink to="/login">
            <button className="login" onClick={() => changeAuthStatus(false)}>
                Logout
            </button>
        </NavLink>
    )
};

export default LogoutBtn;
