import React from 'react'
import {NavLink} from "react-router-dom";

const LoginBtn = () => {
    return (
        <NavLink to="/login">
            <button className="login">
                Login
            </button>
        </NavLink>
    )
};

export default LoginBtn;
