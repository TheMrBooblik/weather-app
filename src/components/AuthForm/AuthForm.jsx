import React from 'react'
import Notification from "./Notification/Notification";
import {useHistory} from "react-router-dom";

function AuthForm(props) {

    const history = useHistory();

    let login = React.createRef();
    let password = React.createRef();

    let submitAuth = (e) => {
        e.preventDefault();
        if (login.current.value.length < 6 || password.current.value.length < 6) {
            props.setNotification("tooShort");
            props.changeAuthStatus(false);
        } else if (login.current.value.length > 100 || password.current.value.length > 100) {
            props.setNotification("tooLong");
            props.changeAuthStatus(false);
        } else {
            if (login.current.value.toLowerCase() === "administrator" && password.current.value === "Dekarta123#") {
                props.setNotification('success');
                props.changeAuthStatus(true);
                e.target.reset();
                history.push('/weather');
            } else {
                props.setNotification("wrongCreds");
                props.changeAuthStatus(false);
            }
        }
    };

    return (
        <form onSubmit={ submitAuth }>
            <p><label> Login: <input type="text" name="login"
                                     ref={login}/></label></p>
            <p><label> Password: <input type="password" name="password"
                                        ref={password}/></label></p>
            <p><input type="submit" value="Submit"/></p>
            <Notification text={props.state.notification}/>
        </form>

    )
}

export default AuthForm;