import React from 'react'
import s from './Notification.module.css'

const Notification = (props) => {

    return (
        <span className={s.notification}>
            {props.text}
        </span>
    )
};

export default Notification;