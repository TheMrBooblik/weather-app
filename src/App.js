import React from 'react';
import './App.css';
import AuthForm from "./components/AuthForm/AuthForm";
import Weather from "./components/Weather/Weather";
import {BrowserRouter, Redirect, Route} from "react-router-dom"
import LoginBtn from "./components/Buttons/LoginBtn/LoginBtn";
import LogoutBtn from "./components/Buttons/LogoutBtn/LogoutBtn";

function App(props) {

    return (
        <BrowserRouter>
            <div className="app">
                <Route path='/login' render={ () => <LoginBtn /> } />
                <Route exact path='/weather' render={ (p) => <LogoutBtn {...p} /> } />

                <h1>Weather app</h1>

                <div className="appContent">
                    <Route path='/' render={ () => <Redirect to={"/login"} /> } />
                    <Route path='/login' render={ (p) => <AuthForm {...p} state={props.state} changeAuthStatus={props.changeAuthStatus} setNotification={props.setNotification} /> }/>
                    <Route path='/weather' render={ () => <Weather state={props.state} currentWeather={props.currentWeather} /> }/>
                </div>
            </div>
        </BrowserRouter>
    );
}

export default App;
