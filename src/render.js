import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {changeAuthStatus, setWeatherData, setNotification} from "./redux/state";
import axios from "axios";

export let renderEntireTree = (state, authStatus) => {
    let renderApp = () => {
        ReactDOM.render(
            <React.StrictMode>
                <App state={state} currentWeather={state.weather.data} changeAuthStatus={changeAuthStatus} setNotification={setNotification}/>
            </React.StrictMode>,
            document.getElementById('root')
        );
    };

    if (authStatus) {
        axios.get(state.weather.url + state.weather.city + '&units=metric&appid=' + state.weather.apiKey)
            .then(function (response) {
                setWeatherData(response.data);
                renderApp();
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    } else {
        renderApp();
    }


};
